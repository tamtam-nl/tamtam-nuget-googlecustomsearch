# README #

This package allows to set up quickly Google Search in an Umbraco site.

### PACKAGE ###

The package adds the following items to your solution:

* 2 .cs files that submit queries to Google and manages the returned XML to be rendered by the AMcropartial
* A Macropartial that can be used immediatly on a Search Template page. Just needs a property for the GSE Key (internal name should be: googleSearchKey)

### ISSUES ADDRESSED ###

* Release 1.0.0 no issues (yet) 