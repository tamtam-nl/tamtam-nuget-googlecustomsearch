﻿using System;

namespace TamTam.NuGet.Umb.GoogleCustomSearch
{
    public class GoogleSearchResult
    {
        public string Title;
        public string URL;
        public string Description;
        public string Type;

        public GoogleSearchResult(string pTitle, string pUrl, string pDescription, string pType)
        {
            Title = pTitle;
            URL = pUrl;
            Description = pDescription;
            Type = pType;
            
        }
    }
}