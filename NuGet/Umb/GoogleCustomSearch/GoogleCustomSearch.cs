﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Text.RegularExpressions;

namespace TamTam.NuGet.Umb.GoogleCustomSearch
{
    public class GoogleCustomSearch
    {
        public string cskey;
        public string keyword;
        public int start;
        public int totalResults;
        public int totalPages;
        public List<GoogleSearchResult> SearchResults;
        public string domain; //If not empty get results form that domain, otherwise gets all
        public string gseURL = @"https://www.google.com/cse?cx={0}&q={1}&output=xml_no_dtd&page={2}";

        public GoogleCustomSearch(string pKey, string pKeyword, string pStart)
        {
            cskey = pKey;
            keyword = pKeyword;
            totalResults = 0;
            if (pStart != string.Empty && pStart != null)
            {
                start = int.Parse(pStart);
            }
            else
            {
                start = 0;
            }
            domain = string.Empty;
        }

        //With Domain
        public GoogleCustomSearch(string pKey, string pKeyword, string pStart, string pDomain) : this(pKey, pKeyword, pStart)
        {
            cskey = pKey;
            keyword = pKeyword;
            totalResults = 0;
            if (pStart != string.Empty && pStart != null)
            {
                start = int.Parse(pStart);
            }
            else
            {
                start = 0;
            }
            domain = pDomain;

            gseURL += "&as_sitesearch={3}";

        }

        public string PagingPreviousURL()
        {
            return HttpContext.Current.Request.PathInfo + "?keyword=" + keyword + "&page=" + (this.start - 1);
        }

        public string PagingNextURL()
        {
            return HttpContext.Current.Request.Path + "?keyword=" + keyword + "&page=" + (this.start + 1);
        }

        public void DoSearch()
        {
            //Make search
            if (keyword != string.Empty)
            {
                //Get Google search results XML 
                XmlDocument xdoc = new XmlDocument();//xml doc used for xml parsing

                string queryURL = string.Format(gseURL, cskey, keyword, start*10, domain);

                xdoc.Load(queryURL);

                //Get total results count
                string TotalResults = string.Empty;
                try
                {
                    TotalResults = xdoc.GetElementsByTagName("M")[0].InnerText;
                }
                catch (Exception ex)
                {
                    TotalResults = "0";
                }
                
                this.totalResults  = int.Parse(TotalResults);
                this.totalPages = int.Parse(Math.Floor(double.Parse(TotalResults.ToString()) / 10).ToString()); 

                //Get all result nodes
                XmlNodeList xNodelst = xdoc.GetElementsByTagName("R");

                if (xNodelst.Count > 0)
                {
                    this.SearchResults = new List<GoogleSearchResult>();

                    string results = string.Empty; int count = 0;

                    foreach (XmlNode xNode in xNodelst)//traversing XML 
                    {
                        string type = string.Empty;
                        if (xNode.Attributes["MIME"] != null)
                        {
                            type = xNode.Attributes["MIME"].Value.Replace("application/", ""); ;
                        }
                        else
                            type = "page";

                        //remove breaklines
                        string description = xNode["S"].InnerText;
                        if ( description != string.Empty )
                        {
                            description = description.Replace("<br>", "");
                        }

                        GoogleSearchResult result = new GoogleSearchResult(xNode["T"].InnerText, xNode["U"].InnerText, description, type);
                        this.SearchResults.Add(result);
                    }
                }

            }
        }

        public string GeneratePaging()
        {
            int totalPages = int.Parse(Math.Floor(double.Parse(totalResults.ToString()) / 10).ToString()); ;
            int current = start;

            string html = "<div id=\"paging\">";

            if (totalPages > 1)
            {
                //Vorige
                if (current > 0)
                    html += "<div class=\"vorige\"><a href=\"?keyword=" + keyword + "&page=" + (current - 1) + "\">Vorige</a></div>";

                //Paging: shows only 5 index pages
                html += "<ul>";

                int indexStart = (current + 1) - 5;
                if (indexStart < 0)
                    indexStart = 0;

                //Back 5
                if ((current - 5) > 0)
                {
                    html += "<li><a title =\"Toon vorige 5 paginas\"  href=\"?keyword=" + keyword + "&page=" + (current - 5) + "\">&#60;&#60;</a></li>";
                }

                //Create paging
                for (int i = 0; i < 5; i++)
                {
                    string active = string.Empty;
                    if (indexStart == current)
                    {
                        active = " class=\"active\" ";
                    }
                    html += "<li " + active + "><a  href=\"?keyword=" + keyword + "&page=" + indexStart + "\">" + (indexStart + 1) + "</a></li>";

                    indexStart++;
                }

                //Forward 5
                if (current < totalPages)
                {
                    int maxPages = current + 5;

                    if (maxPages > totalPages)
                        maxPages = totalPages;

                    html += "<li><a title =\"Toon volgende 5 paginas\" href=\"?keyword=" + keyword + "&page=" + maxPages + "\">&#62;&#62;</a></li>";
                }
                html += "</ul>";

                //Volgende
                if (current < totalPages)
                    html += "<div class=\"volgende\"><a href=\"?keyword=" + keyword + "&page=" + (current + 1) + "\">Volgende</a></div>";
            }
            html += "</div><div class=\"clear\"></div>";

            return html;
        }

       
    }
}